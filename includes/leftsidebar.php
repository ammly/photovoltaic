<div class="row">
    <div class="col-md-2">
        <?php
        include 'widgets/filters.php';
        ?>


        <p class="lead">Photovoltaic</p>
        <div class="list-group">
            <a href="#" class="list-group-item">Residential</a>
            <a href="#" class="list-group-item">Commercial</a>
            <a href="#" class="list-group-item">Industrial Off-Grid</a>
            <a href="#" class="list-group-item">Battery Back-up</a>
        </div>
    </div>