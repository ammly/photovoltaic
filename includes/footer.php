</div>

<div class="col-md-12 page-footer text-center" id="footer">&copy; Copyright 2014-2015 Photovoltaic</div>

</div>
</div>
</div>

<script>
    function detailsModal(id){
        var data = {"id" : id};
        $.ajax({
            url : '/photovoltaic/includes/detailsmodal.php',
            method : "post",
            data : data,
            success : function(data){
                $('body').append(data);
                $('#details-modal').modal('toggle');
            },
            error : function(){
                alert("Something went wrong!");
            }
        });
    }

    function update_cart(mode, edit_id, edit_size){
        var data = {"mode" : mode, "edit_id" : edit_id, "edit_size": edit_size};
        $.ajax({
            url : '/photovoltaic/admin/parsers/update_cart.php',
            method : "post",
            data : data,
            success : function(){location.reload();},
            error : function(){alert("Something went wrong.");},
        });
    }

    function add_to_cart(){
        $('#modal_errors').html('');
        var size = $('#size').val();
        var quantity = $('#quantity').val();
        var available = $('#available').val();
        var error = '';
        var data = $('#add_product_form').serialize();

        if(size == '' || quantity == '' || quantity == 0){
            error += '<p class = "text-danger text-center">You must choose a size and quantity</p>';
            $('#modal_errors').html(error);
            return;
        }else if(quantity > available){
            error += '<p class = "text-danger text-center">There are only <b>'+available+'</b> available. </p>';
            $('#modal_errors').html(error);
            return;
        }else{
            $.ajax({
                url : '/photovoltaic/admin/parsers/add_to_cart.php',
                method : 'post',
                data : data,
                success : function(){
                    location.reload();
                },
                error : function(){alert('Something went wrong.');}
            });
        }
    }
</script>
</body>
</html>
