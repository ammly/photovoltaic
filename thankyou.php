<?php
require_once 'core/init.php';

// Set your secret key: remember to change this to your live secret key in production
// See your keys here https://dashboard.stripe.com/account/apikeys
\Stripe\Stripe::setApiKey(STRIPE_PRIVATE);

// Get the credit card details submitted by the form
$token = $_POST['stripeToken'];

//Get the rest of post data
$full_name = sanitize($_POST['full_name']);
$email = sanitize($_POST['email']);
$street = sanitize($_POST['street']);
$street2 = sanitize($_POST['street2']);
$city = sanitize($_POST['city']);
$county = sanitize($_POST['county']);
$zip_code = sanitize($_POST['zip_code']);
$country = sanitize($_POST['country']);
$tax = sanitize($_POST['tax']);
$sub_total = sanitize($_POST['sub_total']);
$grand_total = sanitize($_POST['grand_total']);
$cart_id = sanitize($_POST['cart_id']);
$description = sanitize($_POST['description']);
$charge_amount = number_format($grand_total,2) * 100000; //amount in shillings
$meta_data = array(
    'cart_id'   =>  $cart_id,
    'tax'       =>  $tax,
    'sub_total' =>  $sub_total
);

// Create the charge on Stripe's servers - this will charge the user's card
try {
    $charge = \Stripe\Charge::create(array(
        "amount"        => $charge_amount, // amount in shillings, again
        "currency"      => CURRENCY,
        "source"        => $token,
        "description"   => $description,
        "receipt_email" => $email,
        "metadata"     => $meta_data
    ));

    //Adjust inventory
    $itemQ = $db->query("SELECT * FROM cart WHERE id = '{$cart_id}'");
    $results = mysqli_fetch_assoc($itemQ);
    $items = json_decode($results['items'],true);
    foreach ($items as $item) {
        $newSizes = array();
        $item_id = $item['id'];
        $productQ = $db->query("SELECT sizes FROM products WHERE Product_id = '{$item_id}'");
        $product = mysqli_fetch_assoc($productQ);
        $sizes = sizesToArray($product['sizes']);
        foreach ($sizes as $size) {
            if($size['size'] == $item['size']){
                $q = $size['quantity'] - $item['quantity'];
                $newSizes[] = array('size' => $size['size'], 'quantity' => $q);
            }else{
                $newSizes[] = array('size' => $size['size'], 'quantity' => $size['quantity']);
            }
        }
        $sizeString = sizesToString($newSizes);
        $db->query("UPDATE products SET sizes = '{$sizeString}' WHERE product_id = '{$item_id}'");

    }


    //Update Cart
    $db->query("UPDATE cart SET paid = 1 WHERE id = '{$cart_id}'");
    $db->query("INSERT INTO transactions
            (charge_id, cart_id, full_name, email, street, street2, city, county, zip_code, country, sub_total, tax, grand_total, description, txn_type)
            VALUES ('$charge->id', '$cart_id', '$full_name', '$email', '$street', '$street2', '$city', '$county', '$zip_code', '$country', '$sub_total', '$tax', '$grand_total', '$description', '$charge->object' )
            ");

    //TODO: fix this to destroy the cookie
    $domain = (($_SERVER['HTTP_HOST'] != 'localhost')?'.'.$_SERVER['HTTP_HOST']:false);
    setcookie(CART_COOKIE, '', 1, '/', $domain, false);
    include 'includes/head.php';
    include 'includes/navigation.php';
    ?>

    <div class="panel panel-success">
        <div class="panel-heading">
            <h3 class="panel-title">Thank You</h3>
        </div>
        <div class="panel-body">
            <p>Your cart has been successfully charged <strong><?=money($grand_total)?></strong>. You have been emailed a
                Receipt. Please check your spam folder if it's not in your inbox. Additionally, you can print this page as a receipt.
            </p>
            <p>Your receipt number is <strong><?=$cart_id;?></strong></p>
            <p>Your order will be shipped to the following address.</p>
            <address>
                <?=$full_name;?><br>
                <?=$street;?><br>
                <?=(($street2 != '')?$street2.'<br>':'');?>
                <?=$city. ', '.$county. ' '.$zip_code;?><br>
                <?=$country;?><br>
            </address
        </div>
        <div class="panel-footer">photovoltaic</div>
    </div>


<?php
    include 'includes/footer.php';


} catch(\Stripe\Error\Card $e) {
    // The card has been declined
//    TODO: Make this prettier. Use api guide.
    //echo $e;
    // Since it's a decline, \Stripe\Error\Card will be caught
  $body = $e->getJsonBody();
  $err  = $body['error'];

  print('Status is:' . $e->getHttpStatus() . "\n");
  print('Type is:' . $err['type'] . "\n");
  print('Code is:' . $err['code'] . "\n");
  // param is '' in this case
  print('Param is:' . $err['param'] . "\n");
  print('Message is:' . $err['message'] . "\n");
} catch (\Stripe\Error\RateLimit $e) {
  // Too many requests made to the API too quickly
} catch (\Stripe\Error\InvalidRequest $e) {
  // Invalid parameters were supplied to Stripe's API
} catch (\Stripe\Error\Authentication $e) {
  // Authentication with Stripe's API failed
  // (maybe you changed API keys recently)
} catch (\Stripe\Error\ApiConnection $e) {
  // Network communication with Stripe failed
} catch (\Stripe\Error\Base $e) {
  // Display a very generic error to the user, and maybe send
  // yourself an email
} catch (Exception $e) {
  // Something else happened, completely unrelated to Stripe
}
?>