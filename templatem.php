<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
  	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0"/>
    <title>Photovoltaic</title>
    <link rel="stylesheet" href="css/materialize/css/materialize.min.css">
    <!-- <link rel="stylesheet" href="css/main.css"> -->
    <link rel="stylesheet" href="http://cdnjs.cloudflare.com/ajax/libs/fotorama/4.6.4/fotorama.css">
</head>
<body>
<!-- Dropdown Structure -->
<ul id="dropdown1" class="dropdown-content">
  <li><a href="#!">one</a></li>
  <li><a href="#!">two</a></li>
  <li class="divider"></li>
  <li><a href="#!">three</a></li>
</ul>
<nav class="teal">
  <div class="nav-wrapper">
    <a href="#!" class="brand-logo">Logo</a>
    <ul class="right hide-on-med-and-down">
      <li><a href="sass.html">Sass</a></li>
      <li><a href="badges.html">Components</a></li>
      <!-- Dropdown Trigger -->
      <li><a class="dropdown-button" href="#!" data-activates="dropdown1">Dropdown<i class="material-icons right">arrow_drop_down</i></a></li>
    </ul>
  </div>
</nav>
<div class="row">
	<div class="col s2">
		<h4 class="text-center">Search By:</h4>
                <h5 class="text-center">Price</h5>
                <form action="search.php" method="post" class="col s2">
                    <div class="row">
                        <!-- <input type="hidden" name="cat" value=""> -->
                        <!-- <input type="hidden" name="price_sort" value="0"> -->
                        <div class="input-field">
                        	<input type="radio" name="price_sort" id="low2hi" value="low">
                        	<label for="low2hi">Low-High</label>
                        </div>
                        <div class="input-field">
                        	<input type="radio" name="price_sort" id="hi2low" value="low">
                        	<label for="hi2low">High - Low</label>
                        </div>
                        <div class="input-field">
                        	<input type="text" name="min_price" class="price-range" placeholder="Min $" value="">To
                        	<input type="text" name="max_price" class="price-range" placeholder="Max $" value=""><br><br>
                        </div>

                        <h4 class="text-center">Brand</h4>
                        	<div class="input-field">
                        		<input type="radio" name="brand" id="all" value="" checked>
                        		<label for="all">All</label>
                        	</div>
                        	<div class="input-field">
                        		<input type="radio" name="brand" id="brand" value="1">
                        		<label for="brand">Brand 1</label>
                        	</div>
                        	<div class="input-field">
                        		<input type="radio" name="brand" id="brand" value="2">
                        		<label for="brand">Brand 2</label>
                        	</div>
                        	<div class="input-field">
                        		<input type="radio" name="brand" id="brand" value="3">
                        		<label for="brand">Brand 3</label>
                        	</div>
                        	<div class="input-field">
                        		<input type="radio" name="brand" id="brand" value="4">
                        		<label for="brand">Brand 4</label>
                        	</div>
                        	<div class="input-field">
                        		<input type="radio" name="brand" id="brand" value="11">
                        		<label for="brand">Kiran&#039;s Brand</label>
                        	</div>
                        	<div class="input-field">
                        		<input type="submit" value="search" class="btn btn-xs btn-primary">
                        	</div>
                    </div>
                </form>
	</div>
	<div class="col s8">
			<div class="col s3">
	          <div class="card">
	            <div class="card-image">
	              <img src="/photovoltaic/images/products/47e95ed75804b5f27989e0c5a792e63f.jpg">
	              <span class="card-title">Commercial Product</span>
	            </div>
	            <div class="card-content">
	              	<p class="list-price text-danger">List Price <s>Ksh. 40000.00</s></p>
                	<p class="price">Our Price: Ksh. 35000.00</p>
	            </div>
	            <div class="card-action">
	              <button type="button" class="btn btn-sm modal-trigger" data-target="modal1" >Details</button>
	            </div>
	          </div>
	        </div>
	        <div class="col s3">
	          <div class="card">
	            <div class="card-image">
	              <img src="/photovoltaic/images/products/47e95ed75804b5f27989e0c5a792e63f.jpg">
	              <span class="card-title">Commercial Product</span>
	            </div>
	            <div class="card-content">
	              	<p class="list-price text-danger">List Price <s>Ksh. 40000.00</s></p>
                	<p class="price">Our Price: Ksh. 35000.00</p>
	            </div>
	            <div class="card-action">
	              <button type="button" class="btn btn-sm modal-trigger" data-target="modal1" >Details</button>
	            </div>
	          </div>
	        </div>
	        <div class="col s3">
	          <div class="card">
	            <div class="card-image">
	              <img src="/photovoltaic/images/products/47e95ed75804b5f27989e0c5a792e63f.jpg">
	              <span class="card-title">Commercial Product</span>
	            </div>
	            <div class="card-content">
	              	<p class="list-price text-danger">List Price <s>Ksh. 40000.00</s></p>
                	<p class="price">Our Price: Ksh. 35000.00</p>
	            </div>
	            <div class="card-action">
	              <button type="button" class="btn btn-sm modal-trigger" data-target="modal1" >Details</button>
	            </div>
	          </div>
	        </div>
	        <div class="col s3">
	          <div class="card">
	            <div class="card-image">
	              <img src="/photovoltaic/images/products/47e95ed75804b5f27989e0c5a792e63f.jpg">
	              <span class="card-title">Commercial Product</span>
	            </div>
	            <div class="card-content">
	              	<p class="list-price text-danger">List Price <s>Ksh. 40000.00</s></p>
                	<p class="price">Our Price: Ksh. 35000.00</p>
	            </div>
	            <div class="card-action">
	              <button type="button" class="btn btn-sm modal-trigger" data-target="modal1" >Details</button>
	            </div>
	          </div>
	        </div>
	</div>
	<div class="col s2">
		<h5 class="text-center">Shopping Cart</h5>
            <div>
                <table class="table table-condensed bordered highlight" id="cart_widget">
                    <tbody>
                        <tr>
                            <td>1</td>
                            <td>Big Solar</td>
                            <td>KSH 4,000.99</td>
                        </tr>
                        <tr>
                            <td></td>
                            <td><b>Sub Total</b></td>
                            <td>KSH 4,000.99</td>
                        </tr>
                    </tbody>
                </table>
                <a href="cart.php" class="btn btn-xs btn-primary pull-right">View Cart</a>
                <div class="clearfix"></div>
            </div>
	</div>
</div>
<footer class="page-footer teal">
          <div class="container">
            <div class="row">
              <div class="col l6 s12">
                <h5 class="white-text">Footer Content</h5>
                <p class="grey-text text-lighten-4">You can use rows and columns here to organize your footer content.</p>
              </div>
              <div class="col l4 offset-l2 s12">
                <h5 class="white-text">Links</h5>
                <ul>
                  <li><a class="grey-text text-lighten-3" href="#!">Link 1</a></li>
                  <li><a class="grey-text text-lighten-3" href="#!">Link 2</a></li>
                  <li><a class="grey-text text-lighten-3" href="#!">Link 3</a></li>
                  <li><a class="grey-text text-lighten-3" href="#!">Link 4</a></li>
                </ul>
              </div>
            </div>
          </div>
          <div class="footer-copyright">
            <div class="container">
            © 2014 Copyright Text
            <a class="grey-text text-lighten-4 right" href="#!">More Links</a>
            </div>
          </div>
 </footer>
 <!-- Modal Structure -->
  <div id="modal1" class="modal modal-fixed-footer">
    <div class="modal-content">
      <h4>Modal Header</h4>
      <p>A bunch of text</p>
    </div>
    <div class="modal-footer">
      <a href="#!" class="modal-action modal-close waves-effect waves-green btn-flat ">Agree</a>
    </div>
  </div>


	<script src="js/jquery.min.js"></script>
    <script src="css/materialize/js/materialize.min.js"></script>
    <script>
    	$(document).ready(function(){
		    // the "href" attribute of .modal-trigger must specify the modal ID that wants to be triggered
		    $('.modal-trigger').leanModal();
		  });
     
    </script>
    <script type="text/javascript" src="https://js.stripe.com/v2/"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/fotorama/4.6.4/fotorama.js"></script>
<!-- fotoramam js-->
</body>
</html>