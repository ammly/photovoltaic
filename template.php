
<!DOCTYPE html>
<html>
<head>
    <title>Photovoltaic</title>
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/materialize/css/materialize.min.css">
    <link rel="stylesheet" href="css/main.css">
<!-- fotorama css -->
    <link rel="stylesheet" href="http://cdnjs.cloudflare.com/ajax/libs/fotorama/4.6.4/fotorama.css">
    <meta name="viewport" content="width=device-width, initial-scale=1,user-scalable=no">
</head>
<body>
<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
            <nav class="navbar navbar-default navbar-fixed-top">
                <div class="container">
                    <a href="index.php" class="navbar-brand">Photovoltaic</a>
                    <ul class="nav navbar-nav">
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">Battery Backup<span class="caret"></span></a>
                            <ul class="dropdown-menu" role="menu">
                                <li><a href="category.php?cat=5">Battery Backup Type 1</a></li>
                                <li><a href="category.php?cat=6">Battery Backup Type 2</a></li>
                                <li><a href="category.php?cat=7">Battery Backup Type 3</a></li>
                            </ul>
                        </li>                                                    
                        <!--TODO: Fix cart value on the badge-->
                        <li><a href="cart.php"><span class="glyphicon glyphicon-shopping-cart badge"> </span> My Cart</a></li>
                    </ul>
                </div>
            </nav>
            <div class="row">
                <div class="col-md-2">
                    <h4 class="text-center">Sort By:</h4>
                    <h5 class="text-center">Price</h5>
                    <form action="search.php" method="post" class="col s2">
                        <div class="row">
                            <!-- <input type="hidden" name="cat" value=""> -->
                            <!-- <input type="hidden" name="price_sort" value="0"> -->
                            <div class="input-field">
                                <input type="radio" name="price_sort" id="low2hi" value="low">
                                <label for="low2hi">Low-High</label>
                            </div>
                            <div class="input-field">
                                <input type="radio" name="price_sort" id="hi2low" value="low">
                                <label for="hi2low">High - Low</label>
                            </div>
                            <div class="input-field">
                                <input type="text" name="min_price" class="price-range" placeholder="Min $" value="">To
                                <input type="text" name="max_price" class="price-range" placeholder="Max $" value=""><br><br>
                            </div>

                            <h4 class="text-center">Brand</h4>
                                <div class="input-field">
                                    <input type="radio" name="brand" id="all" value="" checked>
                                    <label for="all">All</label>
                                </div>
                                <div class="input-field">
                                    <input type="radio" name="brand" id="brand" value="1">
                                    <label for="brand">Brand 1</label>
                                </div>
                                <div class="input-field">
                                    <input type="radio" name="brand" id="brand" value="2">
                                    <label for="brand">Brand 2</label>
                                </div>
                                <div class="input-field">
                                    <input type="radio" name="brand" id="brand" value="3">
                                    <label for="brand">Brand 3</label>
                                </div>
                                <div class="input-field">
                                    <input type="radio" name="brand" id="brand" value="4">
                                    <label for="brand">Brand 4</label>
                                </div>
                                <div class="input-field">
                                    <input type="radio" name="brand" id="brand" value="11">
                                    <label for="brand">Kiran&#039;s Brand</label>
                                </div>
                                <div class="input-field">
                                    <input type="submit" value="search" class="btn btn-xs btn-primary">
                                </div>
                        </div>
                    </form>
                </div>                    
                <!--Main Content-->
                <div class="col-md-8">
                    <h3 class="text-center">Feature Products</h3>
                    <div class="col-md-3">
                        <h5>Commercial</h5>
                          <img src="/photovoltaic/images/products/47e95ed75804b5f27989e0c5a792e63f.jpg" alt="Commercial Product" class="img-thumbnail"/>
                        <p class="list-price text-danger">List Price <s>Ksh. 40000.00</s></p>
                        <p class="price">Our Price: Ksh. 35000.00</p>
                        <button type="button" class="btn btn-sm btn-success" onclick="detailsModal(3)" >Details</button>
                    </div>
                    <div class="col-md-3">
                        <h5>Big Solar</h5>
                            <img src="/photovoltaic/images/products/7e278eab30f2ae41a4b40a7df1232036.png" alt="Big Solar" class="img-thumbnail"/>
                        <p class="list-price text-danger">List Price <s>Ksh. 5000.99</s></p>
                        <p class="price">Our Price: Ksh. 4000.99</p>
                        <button type="button" class="btn btn-sm btn-success" onclick="detailsModal(5)" >Details</button>
                    </div>
                    <div class="col-md-3">
                        <h5>Small Solar</h5>
                            <img src="/photovoltaic/images/products/b47e76c7e3e922bbc2d09f2d79fa8f3f.jpg" alt="Small Solar" class="img-thumbnail"/>
                        <p class="list-price text-danger">List Price <s>Ksh. 60000.00</s></p>
                        <p class="price">Our Price: Ksh. 50000.00</p>
                        <button type="button" class="btn btn-sm btn-success" onclick="detailsModal(6)" >Details</button>
                    </div>
                    <div class="col-md-3">
                        <h5>Small Solar</h5>
                            <img src="/photovoltaic/images/products/b47e76c7e3e922bbc2d09f2d79fa8f3f.jpg" alt="Small Solar" class="img-thumbnail"/>
                        <p class="list-price text-danger">List Price <s>Ksh. 60000.00</s></p>
                        <p class="price">Our Price: Ksh. 50000.00</p>
                        <button type="button" class="btn btn-sm btn-success" onclick="detailsModal(6)" >Details</button>
                    </div>
                    <div class="col-md-3">
                        <h5>Commercial</h5>
                          <img src="/photovoltaic/images/products/47e95ed75804b5f27989e0c5a792e63f.jpg" alt="Commercial Product" class="img-thumbnail"/>
                        <p class="list-price text-danger">List Price <s>Ksh. 40000.00</s></p>
                        <p class="price">Our Price: Ksh. 35000.00</p>
                        <button type="button" class="btn btn-sm btn-success" onclick="detailsModal(3)" >Details</button>
                    </div>
                    <div class="col-md-3">
                        <h5>Big Solar</h5>
                            <img src="/photovoltaic/images/products/7e278eab30f2ae41a4b40a7df1232036.png" alt="Big Solar" class="img-thumbnail"/>
                        <p class="list-price text-danger">List Price <s>Ksh. 5000.99</s></p>
                        <p class="price">Our Price: Ksh. 4000.99</p>
                        <button type="button" class="btn btn-sm btn-success" onclick="detailsModal(5)" >Details</button>
                    </div>
                    <div class="col-md-3">
                        <h5>Small Solar</h5>
                            <img src="/photovoltaic/images/products/b47e76c7e3e922bbc2d09f2d79fa8f3f.jpg" alt="Small Solar" class="img-thumbnail"/>
                        <p class="list-price text-danger">List Price <s>Ksh. 60000.00</s></p>
                        <p class="price">Our Price: Ksh. 50000.00</p>
                        <button type="button" class="btn btn-sm btn-success" onclick="detailsModal(6)" >Details</button>
                    </div>
                    <div class="col-md-3">
                        <h5>Small Solar</h5>
                            <img src="/photovoltaic/images/products/b47e76c7e3e922bbc2d09f2d79fa8f3f.jpg" alt="Small Solar" class="img-thumbnail"/>
                        <p class="list-price text-danger">List Price <s>Ksh. 60000.00</s></p>
                        <p class="price">Our Price: Ksh. 50000.00</p>
                        <button type="button" class="btn btn-sm btn-success" onclick="detailsModal(6)" >Details</button>
                    </div>
                    <div class="col-md-3">
                        <h5>Commercial</h5>
                          <img src="/photovoltaic/images/products/47e95ed75804b5f27989e0c5a792e63f.jpg" alt="Commercial Product" class="img-thumbnail"/>
                        <p class="list-price text-danger">List Price <s>Ksh. 40000.00</s></p>
                        <p class="price">Our Price: Ksh. 35000.00</p>
                        <button type="button" class="btn btn-sm btn-success" onclick="detailsModal(3)" >Details</button>
                    </div>
                    <div class="col-md-3">
                        <h5>Big Solar</h5>
                            <img src="/photovoltaic/images/products/7e278eab30f2ae41a4b40a7df1232036.png" alt="Big Solar" class="img-thumbnail"/>
                        <p class="list-price text-danger">List Price <s>Ksh. 5000.99</s></p>
                        <p class="price">Our Price: Ksh. 4000.99</p>
                        <button type="button" class="btn btn-sm btn-success" onclick="detailsModal(5)" >Details</button>
                    </div>
                    <div class="col-md-3">
                        <h5>Small Solar</h5>
                            <img src="/photovoltaic/images/products/b47e76c7e3e922bbc2d09f2d79fa8f3f.jpg" alt="Small Solar" class="img-thumbnail"/>
                        <p class="list-price text-danger">List Price <s>Ksh. 60000.00</s></p>
                        <p class="price">Our Price: Ksh. 50000.00</p>
                        <button type="button" class="btn btn-sm btn-success" onclick="detailsModal(6)" >Details</button>
                    </div>
                    <div class="col-md-3">
                        <h5>Small Solar</h5>
                            <img src="/photovoltaic/images/products/b47e76c7e3e922bbc2d09f2d79fa8f3f.jpg" alt="Small Solar" class="img-thumbnail"/>
                        <p class="list-price text-danger">List Price <s>Ksh. 60000.00</s></p>
                        <p class="price">Our Price: Ksh. 50000.00</p>
                        <button type="button" class="btn btn-sm btn-success" onclick="detailsModal(6)" >Details</button>
                    </div>                                             
                </div>

                <!--Right Sidebar-->
                <div class="col-md-2">
                    <h5 class="text-center">Shopping Cart</h5>
                    <div>
                        <table class="table table-condensed bordered highlight" id="cart_widget">
                            <tbody>
                                <tr>
                                    <td>1</td>
                                    <td>Big Solar</td>
                                    <td>KSH 4,000.99</td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td><b>Sub Total</b></td>
                                    <td>KSH 4,000.99</td>
                                </tr>
                            </tbody>
                        </table>
                        <a href="cart.php" class="btn btn-xs btn-primary pull-right">View Cart</a>
                        <div class="clearfix"></div>
                    </div>
                    <h5 class="text-center">Popular Items</h5>
                    <div id="recent_widget">
                        <table class="table table-condensed bordered highlight">
                            <tr>
                                <td>
                                    Big Solar</td>
                                <td>
                                    <a class="text-primary" onclick="detailsModal('5')">View</a>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Commercial Prod</td>
                                <td>
                                    <a class="text-primary" onclick="detailsModal('3')">View</a>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Blueish Product</td>
                                <td>
                                    <a class="text-primary" onclick="detailsModal('11')">View</a>
                                </td>
                            </tr>
                            
                        </table>
                    </div>
                </div>
            </div>

            <div class="col-md-12 page-footer text-center" id="footer">&copy; Copyright 2014-2015 Photovoltaic</div>

        </div>
    </div>
</div>
<script src="js/jquery.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="css/materialize/js/materialize.min.js"></script>
    <script type="text/javascript" src="https://js.stripe.com/v2/"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/fotorama/4.6.4/fotorama.js"></script>
<!-- fotoramam js-->
<script>
    function detailsModal(id){
        var data = {"id" : id};
        $.ajax({
            url : '/photovoltaic/includes/detailsmodal.php',
            method : "post",
            data : data,
            success : function(data){
                $('body').append(data);
                $('#details-modal').modal('toggle');
            },
            error : function(){
                alert("Something went wrong!");
            }
        });
    }

    function update_cart(mode, edit_id, edit_size){
        var data = {"mode" : mode, "edit_id" : edit_id, "edit_size": edit_size};
        $.ajax({
            url : '/photovoltaic/admin/parsers/update_cart.php',
            method : "post",
            data : data,
            success : function(){location.reload();},
            error : function(){alert("Something went wrong.");},
        });
    }

    function add_to_cart(){
        $('#modal_errors').html('');
        var size = $('#size').val();
        var quantity = $('#quantity').val();
        var available = $('#available').val();
        var error = '';
        var data = $('#add_product_form').serialize();

        if(size == '' || quantity == '' || quantity == 0){
            error += '<p class = "text-danger text-center">You must choose a size and quantity</p>';
            $('#modal_errors').html(error);
            return;
        }else if(quantity > available){
            error += '<p class = "text-danger text-center">There are only <b>'+available+'</b> available. </p>';
            $('#modal_errors').html(error);
            return;
        }else{
            $.ajax({
                url : '/photovoltaic/admin/parsers/add_to_cart.php',
                method : 'post',
                data : data,
                success : function(){
                    location.reload();
                },
                error : function(){alert('Something went wrong.');}
            });
        }
    }
</script>
</body>
</html>


