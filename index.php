<?php
require_once 'core/init.php';
include 'includes/head.php';
include 'includes/navigation.php';
//include 'includes/carousel.php';
include 'includes/leftsidebar.php';

$sql = "SELECT * FROM products WHERE featured = 1";
$featured = $db->query($sql);
?>
                    <!--Main Content-->
                    <div class="col-md-8">
                        <h2 class="text-center">Feature Products</h2>
                        <?php while($product = mysqli_fetch_assoc($featured)) : ?>
                        <div class="col-md-3">
                            <h4><?= $product['title']; ?></h4>
                            <?php $photos = explode(',', $product['image']);?>
                            <img src="<?= $photos[0]; ?>" alt="<?= $product['title']; ?>" class="img-thumbnail"/>
                            <p class="list-price text-danger">List Price <s>Ksh. <?= $product['list_price']; ?></s></p>
                            <p class="price">Our Price: Ksh. <?= $product['price']; ?></p>
                            <button type="button" class="btn btn-sm btn-success" onclick="detailsModal(<?=$product['product_id']; ?>)" >Details</button>
                        </div>
                        <?php endwhile; ?>
                    </div>
                    <!--Right Sidebar-->
<?php
     include 'includes/rightsidebar.php';
     include 'includes/footer.php';
?>


