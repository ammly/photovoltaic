<?php
require_once '../core/init.php';
if(!is_logged_in()){
    header('Location: login.php');
}
include 'includes/head.php';
include 'includes/navigation.php';

//Complete Order
if(isset($_GET['complete']) && $_GET['complete'] == 1){
    $cart_id = sanitize($_GET['cart_id']);
    $db->query("UPDATE cart SET shipped = 1 WHERE id = '{$cart_id}'");
    $_SESSION['success_flash'] = "The Order Has Been Completed";
    //header('Location: index.php');
    echo '<meta http-equiv="refresh" content="0;index.php">';
}

$txn_id = sanitize((int)$_GET['txn_id']);
$txnQuery = $db->query("SELECT * FROM transactions WHERE id = '{$txn_id}'");
$txn = mysqli_fetch_assoc($txnQuery);
$cart_id = $txn['cart_id'];

$cartQuery = $db->query("SELECT * FROM cart WHERE id = '{$cart_id}'");
$cart = mysqli_fetch_assoc($cartQuery);
$items = json_decode($cart['items'], true);
$idArray = array();
$products = array();
foreach ($items as $item) {
    $idArray[] = $item['id'];
}
$ids = implode(',', $idArray);
$productQ = $db->query("
    SELECT i.product_id as 'id', i.title as 'title', c.category_id as 'cid', c.category as 'child', p.category as 'parent'
    FROM products i
    LEFT JOIN categories c ON i.categories = c.category_id
    LEFT JOIN categories p ON c.parent = p.category_id
    WHERE i.product_id IN ({$ids})
");
//loop through the products
while($p = mysqli_fetch_assoc($productQ)){
    //loop though each item
    foreach ($items as $item) {
        if($item['id'] == $p['id']){
            $x = $item;
            continue;
        }
    }
    $products[] = array_merge($x, $p);
}
?>

<div class="panel panel-default">
    <div class="panel-heading">
        <h2 class="panel-title panel- text-center"><?=$txn['full_name'];?> Order</h2>
    </div>
    <div class="panel-body">
        <h3 class="text-center">Items Ordered</h3>
        <table class="table table-condensed table-bordered table-striped">
            <thead>
            <th>Quantity</th>
            <th>Title</th>
            <th>Category</th>
            <th>Size</th>
            </thead>
            <tbody>
            <?php foreach ($products as $products):?>
                <tr>
                    <td><?=$products['quantity'];?></td>
                    <td><?=$products['title'];?></td>
                    <td><?=$products['parent'].' ~ '.$products['child'];?></td>
                    <td><?=$products['size'];?></td>
                </tr>
            <?php endforeach;?>
            </tbody>
        </table>

        <div class="row">
            <div class="col-md-6">
                <h3 class="text-center">Order Details</h3>
                <table class="table table-condensed table-striped table-bordered">
                    <tbody>
                    <tr>
                        <td>Sub Total</td>
                        <td><?=money($txn['sub_total']);?></td>
                    </tr>
                    <tr>
                        <td>Tax</td>
                        <td><?=money($txn['tax']);?></td>
                    </tr>
                    <tr>
                        <td>Grand Total</td>
                        <td><?=money($txn['grand_total']);?></td>
                    </tr>
                    <tr>
                        <td>Order Date</td>
                        <td><?=pretty_date($txn['txn_date']);?></td>
                    </tr>
                    </tbody>
                </table>
            </div>
            <div class="col-md-3">
                <h3 class="glyphicon glyphicon-home"></h3>
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">Shipping Address</h3>
                    </div>
                    <div class="panel-body">
                        <address>
                            <?=$txn['full_name'];?><br>
                            <?=$txn['street'];?><br>
                            <?=($txn['street2'] != '')?$txn['street2']. '<br>':'';?>
                            <?=$txn['city'].', '.$txn['county'].' '.$txn['zip_code'];?><br>
                            <?=$txn['country'];?><br>
                        </address>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="panel-footer">
        <a href="index.php" class="btn btn-lg btn-default">Cancel</a>
        <a href="orders.php?complete=1&cart_id=<?=$cart_id;?>" class="btn btn-primary btn-lg">Complete Order</a>
    </div>
</div>


<?php include 'includes/footer.php';?>
