<?php
require_once '../core/init.php';
if(!is_logged_in()){
    login_error_redirect();
}
include 'includes/head.php';
include 'includes/navigation.php';

//get brands from db
$sql = "SELECT * FROM brand ORDER BY brand";
$results = $db->query($sql);

$erros = array();

//Edit brand
if(isset($_GET['edit']) && !empty(edit)){
    $edit_id = (int)$_GET['edit'];
    $edit_id = sanitize($edit_id);
    $sql2 = "SELECT * FROM brand WHERE brand_id ='$edit_id'";
    $edit_result = $db->query($sql2);
    $edtBrand = mysqli_fetch_assoc($edit_result);
}

//Delete Brand
if(isset($_GET['delete']) && !empty($_GET['delete'])){
    $delete_id = (int)$_GET['delete'];
    $delete_id = sanitize($delete_id);
    $sql = "DELETE FROM brand WHERE brand_id = '$delete_id' ";
    $db->query($sql);
    echo '<meta http-equiv="refresh" content="0;brands.php">';
}

//if add from is submitted
if(isset($_POST['add_submit'])){
    $brand = sanitize($_POST['brand']);
    //check if brand is blank
    if($_POST['brand'] == ''){
        $erros[] .= 'You must enter a brand';
    }
    //Check if brand exists in db
    $sql = "SELECT * FROM brand WHERE brand = '$brand' ";
    if(isset($_GET['edit'])){
        $sql = "SELECT * FROM brand WHERE brand = '$brand' AND brand_id != '$edit_id' ";
    }
    $result = $db->query($sql);
    $count = mysqli_num_rows($result);
    if($count > 0){
        $erros[] .= $brand.' already Exists. Please choose another Brand Name.';
    }

    //Display errors
    if(!empty($erros)){
        echo display_errors($erros);
    }else{
        //Add brand to db
        $sql = "INSERT INTO brand (brand) VALUES ('$brand')";
        if(isset($_GET['edit'])){
            $sql = "UPDATE brand SET brand = '$brand' WHERE brand_id = '$edit_id' ";
        }
        $db->query($sql);
        echo '<meta http-equiv="refresh" content="0;brands.php">';
    }
}
?>
<h2 class="text-center">Brands</h2><hr>
<!--Brand form -->
<div class="text-center">
    <form action="brands.php<?=((isset($_GET['edit'])))?'?edit='.$edit_id:''?>" method="post" class="form-inline">
        <div class="form-group">
            <?php
            $brand_value = '';
            if(isset($_GET['edit'])){
                $brand_value = $edtBrand['brand'];
            }else{
                if(isset($_POST['brand'])){
                    $brand_value = sanitize($_POST['brand']);
                }
            }?>
            <lable for = "brand"><?=((isset($_GET['edit']))?'Edit':'Add A')?> Brand:</lable>
            <input type="text" name="brand" id="brand" class="form-control" value="<?=$brand_value;?>">
            <?php if(isset($_GET['edit'])):?>
                <a href="brands.php" class="btn btn-default">Cancel</a>
            <?php endif;?>
            <input type="submit" name="add_submit" value="<?=((isset($_GET['edit']))?'Edit':'Add')?> Brand" class="btn btn-success">
        </div>
    </form>
</div>
<hr>
<table class = "table table-bordered table-striped table-responsive table-auto">
    <thead>
        <th>Edit</th>
        <th>Brand</th>
        <th>Delete</th>
    </thead>
    <tbody>
    <?php while($brand = mysqli_fetch_assoc($results)) : ?>
        <tr>
            <td><a href="brands.php?edit=<?= $brand['brand_id']; ?>" class="btn btn-xs btn-default"><span class="glyphicon glyphicon-pencil"></span></a></td>
            <td><?= $brand['brand']; ?></td>
            <td><a href="brands.php?delete=<?= $brand['brand_id']; ?>" class="btn btn-xs btn-danger"><span class="glyphicon glyphicon-remove-sign"></span></a></td>
        </tr>
    <?php endwhile; ?>
    </tbody>
</table>

<?php include 'includes/footer.php'; ?>
