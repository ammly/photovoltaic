<?php
require_once $_SERVER['DOCUMENT_ROOT'].'/photovoltaic/core/init.php';
include 'includes/head.php';

$email = ((isset($_POST['email']))?sanitize($_POST['email']):'');
$email = trim($email);
$password = ((isset($_POST['password']))?sanitize($_POST['password']):'');
$password = trim($password);
$errors = array();
?>
<style>
    body{
        background-image: url("/photovoltaic/images/commercial/commercial-0.jpg");
        background-size: 100vw 100vh;
        background-attachment: fixed;
    }
</style>

<div id="login-form">
    <div>
        <?php
            if($_POST){
                //form validation
                if(empty($_POST['email']) || empty($_POST['password'])){
                    $errors[] = 'You must provide email and password.';
                }
                //validate email
                if(!filter_var($email, FILTER_VALIDATE_EMAIL)){
                    $errors[] = 'You must enter a valid email.';
                }
                //Check if password is more than 6chars
                if(strlen($password) < 6 ){
                    $errors[] = 'Password must be at least 6 characters.';
                }

                //check if email exists in db
                $query = $db->query("SELECT * FROM users WHERE email = '$email'");
                $user = mysqli_fetch_assoc($query);
                $userCount = mysqli_num_rows($query);
                if($userCount < 1){
                    $errors[] = 'Email doesn\'t exist in our database.';
                }

                if(!password_verify($password,$user['password'])){
                    $errors[] = 'The password does not mach our records. Try again.';
                }

                //check for errors
                if(!empty($errors)){
                    echo display_errors($errors);
                }else{
                    //log user in
                    $user_id = $user['user_id'];
                    login($user_id);
                }

            }
        ?>
    </div>
    <h2 class="text-center">Login</h2><hr>
    <form action="login.php" method="post">
        <div class="form-group">
            <label for="email">Email</label>
            <input type="email" class="form-control" name="email" id="email" value="<?=$email;?>">
        </div>
        <div class="form-group">
            <label for="password">Password</label>
            <input type="password" class="form-control" name="password" id="password" value="<?=$password;?>">
        </div>
        <div class="form-group">
            <input type="submit" value="Login" class="btn btn-primary">
        </div>
    </form>
    <p class="text-right"><a href="/photovoltaic/index.php" alt="Home">Visit Site</a></p>
</div>




