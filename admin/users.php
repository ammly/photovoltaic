<?php
require_once '../core/init.php';
if(!is_logged_in()){
    login_error_redirect();
}
if(!has_permission('admin')){
    $_SESSION['error_flash'] = 'Sorry!! You do not have permission to access that page.';
    echo '<meta http-equiv="refresh" content="0;index.php">';
}
include 'includes/head.php';
include 'includes/navigation.php';

if(isset($_GET['delete'])){
    $delete_id = sanitize($_GET['delete']);
    $db->query("DELETE FROM users WHERE user_id = '$delete_id'");
    $_SESSION['success_flash'] = 'User deleted successifully.';
    //header('Location: index.php');
    echo '<meta http-equiv="refresh" content="0;users.php">';
}

if(isset($_GET['add'])){

    $name = ((isset($_POST['name']))?sanitize($_POST['name']):'');
    $email = ((isset($_POST['email']))?sanitize($_POST['email']):'');
    $password = ((isset($_POST['password']))?sanitize($_POST['password']):'');
    $confirm_password = ((isset($_POST['confirm_password']))?sanitize($_POST['confirm_password']):'');
    $permissions = ((isset($_POST['permission']))?sanitize($_POST['permission']):'');

    $errors = array();
    if($_POST){
        $emailQuery = $db->query("SELECT * FROM users WHERE email = '$email'");
        $emailCount = mysqli_num_rows($emailQuery);
        if($emailCount != 0){
            $errors[] = 'That email is already taken.';
        }
        $required = array('name', 'email', 'password', 'confirm_password', 'permission');
        foreach($required as $field){
            if(empty($_POST[$field])){
                $errors[] = 'You must fill out all fields.';
                break;
            }
        }

        if(strlen($password) < 6){
            $errors[] = 'The password must be at least 6 characters.';
        }

        if($password != $confirm_password){
            $errors[] = 'The password must match Password Confirm.';
        }

        if(!filter_var($email, FILTER_VALIDATE_EMAIL)){
            $errors[] = 'You must enter a valid email';
        }

        if(!empty($errors)){
            echo display_errors($errors);
        }else{
            //add user to db
            $hashed = password_hash($password, PASSWORD_DEFAULT);
            $db->query("INSERT INTO users(full_name, email, password, permissions )
                          VALUES ('$name', '$email', '$hashed','$permissions')");
            $_SESSION['success_flash'] = 'User Has Been Added.';
            //header('Location: users.php');
            echo '<meta http-equiv="refresh" content="0;users.php">';
        }
    }
?>
   <h2 class="text-center">Add New User</h2>
    <form action="users.php?add=1" method="post">
        <div class="form-group col-md-6">
            <label for="name">Full Name:</label>
            <input type="text" class="form-control" id="name" name="name" value="<?=$name;?>">
        </div>
        <div class="form-group col-md-6">
            <label for="email">Email:</label>
            <input type="email" class="form-control" id="email" name="email" value="<?=$email;?>">
        </div>
        <div class="form-group col-md-6">
            <label for="password">Password:</label>
            <input type="password" class="form-control" id="password" name="password" value="<?=$password;?>">
        </div>
        <div class="form-group col-md-6">
            <label for="confirm_password">Confirm Password:</label>
            <input type="password" class="form-control" id="confirm_password" name="confirm_password" value="<?=$confirm_password;?>">
        </div>
        <div class="form-group col-md-6">
            <label for="permission">Permissions:</label>
            <select name="permission" id="permission" class="form-control">
                <option value=""<?=(($permissions = '')?'selected':'');?>></option>
                <option value="editor"<?=(($permissions = 'editor')?'selected':'');?>>Editor</option>
                <option value="admin,editor"<?=(($permissions = 'admin,editor')?'selected':'');?>>Admin</option>
            </select>
        </div>
        <div class="form-group col-md-6 text-right" style="margin-top: 25px;">
            <a href="users.php" class="btn btn-default">Cancel</a>
            <input type="submit" class=" btn btn-primary" value="Add User">
        </div>
    </form>

<?php
}else{

$userQuery = $db->query("SELECT * FROM users ORDER BY full_name");
?>

<h2 class="text-center">Users</h2>
<a href="users.php?add=1" class="btn btn-success pull-right" id="add-product-btn">Add New User</a>
<hr>
<table class="table table-bordered table-striped table-condensed">
    <thead>
        <th></th><th>Name</th><th>Email</th><th>Join Date</th><th>Last Login</th><th>Permissions</th>
    </thead>
    <tbody>
    <?php while($user = mysqli_fetch_assoc($userQuery)):?>
        <tr>
            <td>
            <?php if($user['user_id'] != $user_data['user_id']):?>
                <a href="users.php?delete=<?=$user['user_id'];?>" class="btn btn-default btn-xs"><span class="glyphicon glyphicon-remove-sign"></span></a>
            <?php endif;?>
            </td>
            <td><?=$user['full_name'];?></td>
            <td><?=$user['email'];?></td>
            <td><?=pretty_date($user['join_date']);?></td>
            <td><?=(($user['last_login'] == '0000-00-00 00:00:00')?'Never':pretty_date($user['last_login']));?></td>
            <td><?=$user['permissions'];?></td>
        </tr>
    <?php endwhile;?>
    </tbody>
</table>


<?php } include 'includes/footer.php'; ?>
