<?php
require_once $_SERVER['DOCUMENT_ROOT'].'/photovoltaic/core/init.php';

if(!is_logged_in()){
    login_error_redirect();
}
include 'includes/head.php';
include 'includes/navigation.php';

//Delete product
if(isset($_GET['delete'])){
    $id = sanitize($_GET['delete']);
    $db->query("UPDATE products SET deleted = 1, featured = 0 WHERE product_id = '$id'");
    echo '<meta http-equiv="refresh" content="0;products.php">';
}

$dbPath = '';
if(isset($_GET['add']) || isset($_GET['edit'])){
$brandQuery = $db->query("SELECT * FROM brand ORDER BY brand");
$parentQuery =$db->query("SELECT * FROM categories WHERE parent = 0 ORDER BY category");
$title = ((isset($_POST['title']) && $_POST['title'] != '')?sanitize($_POST['title']):'');
$brand = ((isset($_POST['brand']) && !empty($_POST['brand']))?sanitize($_POST['brand']):'');
$parent = ((isset($_POST['parent']) && !empty($_POST['parent']))?sanitize($_POST['parent']):'');
$category = ((isset($_POST['child']) && !empty($_POST['child']))?sanitize($_POST['child']):'');
$price = ((isset($_POST['price']) && $_POST['price'] != '')?sanitize($_POST['price']):'');
$list_price = ((isset($_POST['list_price']) && $_POST['list_price'] != '')?sanitize($_POST['list_price']):'');
$description = ((isset($_POST['description']) && $_POST['description'] != '')?sanitize($_POST['description']):'');
$sizes = ((isset($_POST['sizes']) && $_POST['sizes'] != '')?sanitize($_POST['sizes']):'');
$sizes = rtrim($sizes,',');
$savedImage = '';
if(isset($_GET['edit'])){
$edit_id = (int)$_GET['edit'];
$productResult = $db->query("SELECT * FROM products WHERE product_id = '$edit_id'");
$product = mysqli_fetch_assoc($productResult);
if(isset($_GET['delete_image'])){
    $imgi = (int)$_GET['imgi'] - 1;
    $images = explode(',', $product['image']);
    $imageUrl = $_SERVER['DOCUMENT_ROOT'].$images[$imgi];
    unset($imageUrl);
    unset($images[$imgi]);
    $imageStr = implode(',', $images);
    $db->query("UPDATE products SET image = '{$imageStr}' WHERE product_id = '$edit_id'");
    //header('Location: products.php?edit='.$edit_id);
    echo '<meta http-equiv="refresh" content="0;products.php?edit='.$edit_id.'>"';
}
$category = ((isset($_POST['child'])) && $_POST['child'] != ''? sanitize($_POST['child']):$product['categories']);
$title = ((isset($_POST['title']) && $_POST['title'] != '')?sanitize($_POST['title']):$product['title']);
$brand = ((isset($_POST['brand']) && $_POST['brand'] != '')?sanitize($_POST['brand']):$product['brand']);
$parentQ = $db->query("SELECT * FROM categories WHERE category_id = '$category'");
$parentResult = mysqli_fetch_assoc($parentQ);
$parent = ((isset($_POST['parent']) && $_POST['parent'] != '')?sanitize($_POST['parent']):$parentResult['parent']);
$price = ((isset($_POST['price']) && $_POST['price'] != '')?sanitize($_POST['price']):$product['price']);
$list_price = ((isset($_POST['list_price']))?sanitize($_POST['list_price']):$product['list_price']);
$description = ((isset($_POST['description']))?sanitize($_POST['description']):$product['description']);
$sizes = ((isset($_POST['sizes']) && $_POST['sizes'] != '')?sanitize($_POST['sizes']):$product['sizes']);
$sizes = rtrim($sizes,',');
$savedImage = (($product['image'] != '')?$product['image']:'');
$dbPath = $savedImage;
}
if(!empty($sizes)){
        $sizeString = sanitize($sizes);
        $sizeString = rtrim($sizeString,',');
        $sizesArray = explode(',',$sizeString);
        $sArray = array();
        $qArray = array();
        $tArray = array();
        foreach ($sizesArray as $ss) {
            $s = explode(':', $ss);
            $sArray[] = $s[0];
            $qArray[] = $s[1];
            $tArray[] = $s[2];
        }
    }else{$sizesArray = array();}

if($_POST){
$errors = array();
    //validation
    $required = array('title', 'price', 'parent', 'child', 'sizes');
    $allowed = array('png','jpg','jpeg','gif');
    $uploadPath = array();
    $tempLoc = array();
    foreach($required as $field){
        if($_POST[$field] == ''){
            $errors[] = 'All Fields with an Asterisk are required.';
            break;
        }
    }
   // var_dump($_FILES['photo']);
    $photo_count = count($_FILES['photo']['name']);
    if($photo_count > 0){
    for($i = 0; $i < $photo_count; $i++){
            $name = $_FILES['photo']['name'][$i];
            $nameArray = explode('.', $name);
            $fileName = $nameArray[0];
            $fileExt = $nameArray[1];
            $mime = explode('/', $_FILES['photo']['type'][$i]);
            $mimeType = $mime[0];
            $mimeExt = $mime[1];
            $tmpLoc[] = $_FILES['photo']['tmp_name'][$i];
            $fileSize = $_FILES['photo']['size'][$i];
            $uploadName = md5(microtime().$i).'.'.$fileExt;
            $uploadPath[] = BASEURL.'images/products/'.$uploadName;
            if($i != 0){
                $dbPath .= ',';
            }
            $dbPath .= '/photovoltaic/images/products/'.$uploadName;
            if($mimeType != 'image'){
                $errors[] = 'The File must be an Image.';
            }
            if(!in_array($fileExt, $allowed)){
                $errors[] = 'Image extension must be a png, jpg, jpeg or gif. ';
            }
            if($fileSize > 15000000){
                $errors[] = 'The File Size must be under 15MB';
            }
            if($fileExt != $mimeExt && ($mimeExt == 'jpeg' && $fileExt != 'jpg')){
                $errors[] = 'File Extension does not match the file';
            }
        }
    }
    if(!empty($errors)){
        echo display_errors($errors);
    }else{
        //Upload file & insert product into db
        if($photo_count > 0){
            for($i = 0; $i < $photo_count; $i++){
                move_uploaded_file($tmpLoc[$i], $uploadPath[$i]);
            }
        }

        $insertSql = "INSERT INTO products (`title`, `price`, `list_price`, `brand`, `categories`, `sizes`, `image`, `description`)
                      VALUES ('$title', '$price', '$list_price', '$brand', '$category','$sizes', '$dbPath', '$description')
                      ";
        if(isset($_GET['edit'])){
            $insertSql = "UPDATE products SET title = '$title', price = '$price', list_price = '$list_price',
                            brand = '$brand', categories = '$category', sizes = '$sizes', image = '$dbPath', description = '$description'
                            WHERE product_id = '$edit_id'
            ";
        }
        if($db->query($insertSql)){
            //header('Location: products.php');
            echo '<meta http-equiv="refresh" content="0;products.php">';
        }else{
            echo $db->error;
        }
    }
}
?>
<div class="container">
    <h2 class="text-center"><?=((isset($_GET['edit']))?'Edit': 'Add A New')?> Product</h2></hr>
<form action="products.php?<?=((isset($_GET['edit']))?'edit='.$edit_id:'add=1');?>" method="post" enctype="multipart/form-data">
    <div class="form-group col-md-3">
       <label for = "title">Title*:</label>
       <input type="text" class="form-control" name="title" id="title" value="<?=$title;?>">
    </div>
    <div class="form-group col-md-3">
        <label for="brand">Brand*:</label>
        <select name="brand" id="brand" class="form-control">
            <option value="<?=(($brand == '' )?'Selected':'');?>"></option>
            <?php while($b = mysqli_fetch_assoc($brandQuery) ):?>
                <option value="<?=$b['brand_id'];?>"<?=(($brand == $b['brand_id'])?'Selected': '');?>><?=$b['brand'];?></option>
            <?php endwhile;?>
        </select>
    </div>
    <div class="form-group col-md-3">
        <label for="parent">Parent Category*:</label>
        <select name="parent" id="parent" class="form-control">
            <option value="<?=(($parent == '')?'Selected':'');?>"></option>
            <?php while($p = mysqli_fetch_assoc($parentQuery)):?>
                <option value="<?=$p['category_id'];?>"<?=(($parent == $p['category_id'])?'Selected':'');?>><?=$p['category'];?></option>
            <?php endwhile;?>
        </select>
    </div>
    <div class="form-group col-md-3">
        <label for="child">Child Category*:</label>
        <select name="child" id="child" class="form-control"></select>
    </div>
    <div class="form-group col-md-3">
        <label for="price">Price*:</label>
        <input type="text" name="price" id="price" class="form-control" value="<?=$price;?>">
    </div>
    <div class="form-group col-md-3">
        <label for="list_price">List Price:</label>
        <input type="text" name="list_price" id="list_price" class="form-control" value="<?=$list_price;?>">
    </div>
    <div class="form-group col-md-3">
        <label>Quantity & Sizes*:</label>
        <button class="btn btn-default form-control" onclick="$('#sizesModal').modal('toggle');return false;">Quantity & Sizes</button>
    </div>
    <div class="form-group col-md-3">
        <label for="sizes">Sizes & Quantity Preview</label>
        <input class="form-control" type="text" name="sizes" id="sizes" value="<?=$sizes;?>"readonly>
    </div>
    <div class="form-group col-md-6">
        <?php if($savedImage != ''):?>
        <?php
            $imgi = 1;
            $images = explode(',', $savedImage);?>

            <?php foreach ($images as $image):?>

            <div class="saved-image col-md-4">
                <img src="<?=$image;?>" class="img-thumbnail" alt="Saved Image"><br>
                <a href="products.php?delete_image=1&edit=<?=$edit_id;?>&imgi=<?=$imgi;?>" class="text-danger">Delete Image</a>
            </div>
            <?php
                $imgi++;
                 endforeach;
            ?>
        <?php else:?>
            <label for="photo">Product Photo</label>
            <input type="file" class="form-control" name="photo[]" id="photo" multiple>
        <?php endif;?>
    </div>
    <div class="form-group col-md-6">
        <label for="description">Description</label>
        <textarea name="description" id="description" class="form-control" rows="6" ><?=$description;?></textarea>
    </div>
    <div class="form-group col-md-3 pull-right">
        <a href="products.php" class=" btn btn-default">Cancel</a>
        <input type="submit" value="<?=((isset($_GET['edit']))?'Edit': 'Add')?>" class=" btn btn-success">
    </div>
    <div class="clearfix"></div>
</form>

<!--Sizes Modal-->
<div class="modal fade" id="sizesModal" tabindex="-1" role="dialog" aria-labelledby="sizesModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="sizesModalLabel">Size & Quantity</h4>
            </div>
            <div class="modal-body">
                <div class="container-fluid">
                   <?for($i=1;$i<=12;$i++):?>
                    <div class="form-group col-md-2">
                        <label for="size<?=$i;?>">Size:</label>
                        <input type="text" name="size<?=$i;?>" id="size<?=$i;?>" class="form-control" value="<?=((!empty($sArray[$i-1]))?$sArray[$i-1]:'')?>">
                    </div>
                    <div class="form-group col-md-2">
                        <label for="quantity<?=$i;?>">Quantity:</label>
                        <input type="number" name="quantity<?=$i;?>" id="quantity<?=$i;?>" class="form-control" value="<?=((!empty($qArray[$i-1]))?$qArray[$i-1]:'')?>" min="0">
                    </div>
                    <div class="form-group col-md-2">
                        <label for="threshold<?=$i;?>">Threshold:</label>
                        <input type="number" name="threshold<?=$i;?>" id="threshold<?=$i;?>" class="form-control" value="<?=((!empty($tArray[$i-1]))?$tArray[$i-1]:'')?>" min="0">
                    </div>
                   <?endfor;?>
                 </div>
            </div>
            <div class="modal-footer">
                <button class="btn btn-default" data-dismiss="modal">Close</button>
                <button class="btn btn-primary" onclick="updateSizes();$('#sizesModal').modal('toggle');return false;">Save Changes</button>
            </div>
        </div>
    </div>
</div>

<?php }else{
$sql = "SELECT * FROM products WHERE deleted = 0";
$presult = $db->query($sql);

//feature a product
if(isset($_GET['featured'])){
    $product_id = (int)$_GET['id'];
    $featured = (int)$_GET['featured'];
    $featuredSql = "UPDATE products
                    SET featured = '$featured'
                    WHERE product_id = '$product_id'";
    $db->query($featuredSql);
    //header('Location: products.php');
    echo '<meta http-equiv="refresh" content="0;products.php">';
}
?>
<h3 class="text-center">Products</h3>
<a href="products.php?add=1" class="btn btn-success pull-right" id="add-product-btn">Add Product</a><div class="clearfix"></div>
</hr>
<table class="table table-bordered table-condensed table-striped">
    <thead>
        <th></th>
        <th>Products</th>
        <th>Price</th>
        <th>Brand</th>
        <th>Category</th>
        <th>Featured</th>
        <th>Sold</th>
    </thead>
    <tbody>
        <?php   while($products = mysqli_fetch_assoc($presult)):
                $childId = $products['categories'];
                $catSql = "SELECT * FROM categories WHERE category_id = '$childId'";
                $chresult = $db->query($catSql);
                $childc = mysqli_fetch_assoc($chresult);

                $parentId = $childc['parent'];
                $parentSql = "SELECT * FROM categories WHERE category_id = '$parentId'";
                $prresult = $db->query($parentSql);
                $parentp = mysqli_fetch_assoc($prresult);
                $category = $parentp['category'] .'~'. $childc['category'];

                $brand_id = $products['brand'];
                $brandQ = $db->query("SELECT brand FROM brand WHERE brand_id = '$brand_id'");
                $brandN = mysqli_fetch_assoc($brandQ);
            ?>
            <tr>
                <td>
                    <a href="products.php?edit=<?=$products['product_id']?>" class="btn btn-xs btn-default"><span class="glyphicon glyphicon-pencil"></span></a>
                    <a href="products.php?delete=<?=$products['product_id']?>" class="btn btn-xs btn-default"><span class="glyphicon glyphicon-remove-sign"></span></a>
                </td>
                <td><?=$products['title'];?></td>
                <td><?=money($products['price']);?></td>
                <td><?=$brandN['brand'];?></td>
                <td><?=$category;?></td>
                <td><a href="products.php?featured=<?=(($products['featured'] == 0)?'1':'0');?>&id=<?=$products['product_id']?>" class="btn btn-xs btn-default">
                        <span class="glyphicon glyphicon-<?=(($products['featured'] == 1)?'minus':'plus');?>"></span>
                    </a>&nbsp; <?=(($products['featured'] == 1)?'Featured Product':'');?></td>
                <td>0</td>
            </tr>
        <?php endwhile;?>
    </tbody>
</table>
</div>

<?php }include 'includes/footer.php'; ?>
<script>
    $('document').ready(function(){
        get_child_options('<?=$category;?>');
    });
</script>
