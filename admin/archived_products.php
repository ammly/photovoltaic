<?php
require_once $_SERVER['DOCUMENT_ROOT'].'/photovoltaic/core/init.php';
if(!is_logged_in()){
    login_error_redirect();
}
include 'includes/head.php';
include 'includes/navigation.php';

$sql = "SELECT * FROM products WHERE deleted = 1";
$archived = $db->query($sql);
if(isset($_GET['restore'])){
    print_r($_GET);
    $product_id = (int)$_GET['restore'];
    $restore = (int)$_GET['restore'];
    $restoreSql = "UPDATE products
                    SET deleted = 0
                    WHERE product_id = '$product_id'";
    $db->query($restoreSql);
    //header('Location: products.php');
    echo '<meta http-equiv="refresh" content="0;archived_products.php">';
}
?>
<h3 class="text-center">Archived Products</h3>
</hr>
<table class="table table-bordered table-condensed table-striped">
    <thead>
        <th>Restore</th>
        <th>Products</th>
        <th>Price</th>
        <th>Category</th>
        <th>Sold</th>
    </thead>
    <tbody>
        <?php while($product = mysqli_fetch_assoc($archived)):
                $childId = $product['categories'];
                $catSql = "SELECT * FROM categories WHERE category_id = '$childId'";
                $result = $db->query($catSql);
                $child = mysqli_fetch_assoc($result);

                $parentId = $child['parent'];
                $parentSql = "SELECT * FROM categories WHERE category_id = '$parentId'";
                $presult = $db->query($parentSql);
                $parent = mysqli_fetch_assoc($presult);
                $category = $parent['category'] .'~'. $child['category'];
            ?>
            <tr>
                <td>
                    <a href="archived_products.php?restore=<?=$product['product_id']?>" class="btn btn-xs btn-default"><span class="glyphicon glyphicon-refresh"></span></a>
                </td>
                <td><?=$product['title'];?></td>
                <td><?=money($product['price']);?></td>
                <td><?=$category;?></td>
                <td>0</td>
            </tr>
        <?php endwhile;?>
    </tbody>
</table>

<?php include 'includes/footer.php'; ?>