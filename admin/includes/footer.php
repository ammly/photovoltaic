</div>

<div class="col-md-12 text-center" id="footer">&copy; Copyright 20014-2015 Photovoltaic</div>

</div>
</div>
</div>
<script>
    function updateSizes(){
        var sizeString ='';
        for(var i=1;i<=12;i++){
            if($('#size'+i).val()!= ''){
                sizeString += $('#size'+i).val()+':'+$('#quantity'+i).val()+':'+$('#threshold'+i).val()+',';
            }
        }
        $('#sizes').val(sizeString);
    }
    function get_child_options(selected){
        if(typeof selected === 'undefined'){
            var selected = '';
        }
        var parentId = $('#parent').val();
        $.ajax({
           url: '/photovoltaic/admin/parsers/child_categories.php',
            type: 'POST',
            data: {parentId: parentId, selected:selected},
            success: function(data){
                $('#child').html(data);
            },
            error: function(){
                alert("Something went wrong with the child options.");
            }
        });
    }
    $('select[name="parent"]').change(function (){
        get_child_options();
    });
</script>
</body>
</html>
