<?php
require_once $_SERVER['DOCUMENT_ROOT'].'/photovoltaic/core/init.php';
if(!is_logged_in()){
    login_error_redirect();
}
include 'includes/head.php';
$old_password = ((isset($_POST['old_password']))?sanitize($_POST['old_password']):'');
$old_password = trim($old_password);

$hashed = $user_data['password'];
$password = ((isset($_POST['password']))?sanitize($_POST['password']):'');
$password = trim($password);

$password_confirm = ((isset($_POST['password_confirm']))?sanitize($_POST['password_confirm']):'');
$password_confirm = trim($password);
$new_hashed = password_hash($password, PASSWORD_DEFAULT);
$user_id = $user_data['user_id'];
$errors = array();
?>

<div id="login-form">
    <div>
        <?php
        if($_POST){
            //form validation
            if(empty($_POST['old_password']) || empty($_POST['password']) || empty($_POST['password_confirm'])){
                $errors[] = 'You must fill out all fields.';
            }
            //Check if password is more than 6chars
            if(strlen($password) < 6 ){
                $errors[] = 'Password must be at least 6 characters.';
            }

            //check if new password matches password_confirm
            if($password != $password_confirm){
                $errors[] = 'New Password and Password_Confirm does not match.';
            }

            if(!password_verify($old_password,$hashed)){
                $errors[] = 'Your old password does not match our records.';
            }

            //check for errors
            if(!empty($errors)){
                echo display_errors($errors);
            }else{
                //Change password
                $db->query("UPDATE users SET password = '$new_hashed' WHERE user_id = '$user_id'");
                $_SESSION['success_flash'] = 'Your password has been updated.';
                echo '<meta http-equiv="refresh" content="0;index.php">';
            }

        }
        ?>
    </div>
    <h2 class="text-center">Change Password</h2><hr>
    <form action="change_password.php" method="post">
        <div class="form-group">
            <label for="old_password">Old Password:</label>
            <input type="password" class="form-control" name="old_password" id="old_password" value="<?=$old_password;?>">
        </div>
        <div class="form-group">
            <label for="password">New Password:</label>
            <input type="password" class="form-control" name="password" id="password" value="<?=$password;?>">
        </div>
        <div class="form-group">
            <label for="password_confirm">Confirm New Password:</label>
            <input type="password" class="form-control" name="password_confirm" id="password_confirm" value="<?=$password_confirm;?>">
        </div>
        <div class="form-group">
            <a href="index.php" class="btn btn-default">Cancel</a>
            <input type="submit" value="Login" class="btn btn-primary">
        </div>
    </form>
    <p class="text-right"><a href="/photovoltaic/index.php" alt="Home">Visit Site</a></p>
</div>




